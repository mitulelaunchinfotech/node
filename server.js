var express = require('express');
const path = require('path');
var http = require('http');
const bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var passport = require('passport');
var session = require('express-session');
const flash = require('connect-flash');
require('dotenv').config()

// Router Prefix Setup 
express.application.prefix = express.Router.prefix = function(path, configure) {
    var router = express.Router();
    this.use(path, router);
    configure(router);
    return router;
};



// const allowedOrigins = [
//     'capacitor://localhost',
//     'ionic://localhost',
//     'http://localhost',
//     'http://localhost:8081',
//     'http://localhost:8100'
//   ];

//   // Reflect the origin if it's in the allowed list or not defined (cURL, Postman, etc.)
//   const corsOptions = {
//     origin: (origin, callback) => {
//       if (allowedOrigins.includes(origin) || !origin) {
//         callback(null, true);
//       } else {
//         callback(new Error('Origin not allowed by CORS'));
//       }
//     }
//   }



const routes = require('./routes');

// Load required DB Model


const app = express();
//   app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(express.static('public'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


// Enable preflight requests for all routes
// app.options('*', cors(corsOptions));


app.use(cookieParser())
app.use(session({
    secret: "keys.sessionKey",
    cookie: {
        expires: 3600000
    },
    saveUninitialized: true,
    resave: true
}));


app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
require('./config/passport')(passport);


app.use('/', routes);


 
app.set('port', '3000');
var server = http.createServer(app);
server.listen('3000');