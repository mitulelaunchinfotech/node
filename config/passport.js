const Localstratagey = require('passport-local');
const md5 = require('md5')
const {
    User
} = require('../connection');

function SessionConstructor(userId, userGroup, details) {
    this.userId = userId;
    this.userGroup = userGroup;
    this.details = details;
}

module.exports = function (passport) {
    passport.serializeUser(function (user, done) {
        let userPrototype = Object.getPrototypeOf(user);
        if (userPrototype === User.prototype) {

            let sessionConstructor = new SessionConstructor(user.id, 'user', '');
            done(null, sessionConstructor);
        }

    });
    passport.deserializeUser(function (sessionConstructor, done) {
        if (sessionConstructor.userGroup == 'user') {
            User.findOne({
                where: {
                    id: sessionConstructor.userId
                }
            }).then(function (user) {
                done(null, user)
            });
        }
    });
    passport.use('admin-local', new Localstratagey({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        async function (req, email, password, done) {

            let responseUser;
            await User.findOne({
                where: {
                    email
                }
            }).then(async function (user) {
                if (!user) {
                    return done( null,false, req.flash('loginMessage', 'Invalid username or password'));                
                }
                if (md5(password) != user.password)
                    return done(null, false, req.flash('loginMessage', 'Oops wrong password'));                    

                return done(null, user);
            });
        }
    ))
}