const mysql = require('mysql');

const createResponse = params => {
	const {
		response,
		code,
		success,
		message,
		data = {},
		error = {}
	} = params;


	if (code == 422 || code == 501 || code == 401) {
		return response.status(code).json({
			success,
			message,
			error
		});
	} else {
		return response.status(code).json({
			success,
			message,
			data
		});
	}
}

const createSuccessResponse = (
		response,
		message = 'success',
		data = {},
		code = 200,
	) =>
	createResponse({
		response,
		code,
		message,
		data
})

const createErrorResponse = (
	request,
	response,
	message = 'Server Internal Error',
	error = {},
	code = 422,
) =>
createResponse({
	response,
	code,
	message,
	error,
})


const DBConnection = () => {
	const db = mysql.createConnection({
		host: "127.0.0.1",
		user: `root`,
		password: ``,
		database: `node`
	});
	return db;
}

module.exports = {
	createSuccessResponse,
	createErrorResponse,
	DBConnection
};