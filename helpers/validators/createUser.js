const Joi = require('joi');


const createUser = Joi.object().keys({
    first_name: Joi.string().required().label('First name is required'),
    last_name: Joi.string().required().label('Last name is required'),
    email: Joi.string().required().label('email is required'),
    email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }).required().label('email extension required'),

})


const loginUser = Joi.object().keys({
    email : Joi.string().required().label('Email is required'),
    password : Joi.string().required().label('Password is required'),

})

const _validationOptions = {
    abortEarly: false,
    stripUnknown: true,
    allowUnknown: true,
};


const createUserValidator = ((payload, req, res) => {
    let validationResult;
    validationResult = createUser.validate(payload, _validationOptions);
    if (validationResult.error) {

        const error = {};
        var detailsArray = validationResult.error.details;

        const message = "Validation Failed";
        detailsArray.forEach(element => {
    
            const key = element['context'].key;
            error[`${key}`] = element['context'].label;
        });

        return { status: true, message: message, error: error };
    }
    return { status: false };
});


const loginUservalidator = ((payload, req, res) => {
    let validationResult;
    validationResult = loginUser.validate(payload, _validationOptions);
    if (validationResult.error) {

        const error = {};
        var detailsArray = validationResult.error.details;

        const message = "Validation Failed";
        detailsArray.forEach(element => {
    
            const key = element['context'].key;
            error[`${key}`] = element['context'].label;
        });

        return { status: true, message: message, error: error };
    }
    return { status: false };
});

module.exports = {
    createUserValidator,
    loginUservalidator
};