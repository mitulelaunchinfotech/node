const mysql = require('mysql');
const Sequelize = require('sequelize')

const UserModel = require('./models/user')

// var mysqlConnection = mysql.createConnection({
//     host:"localhost",
//     user:"root",
//     password:"",
//     database:"node",
//     multipleStatements:true
// })

// mysqlConnection.connect((err)=>{
//     if(!err)
//     {
//         console.log("connected");
//     }else{
//         console.log("failed");
//     }
// })

const sequelize = new Sequelize('node', 'root', '', {
	host: "localhost",
	dialect: 'mysql',
	logging: false,
	pool: {
		max: 10,
		min: 0,
		acquire: 30000, 
		idle: 10000
	}
})

const User = UserModel(sequelize, Sequelize)

module.exports = {
	sequelize,
    User
};