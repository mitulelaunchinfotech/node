const jwt = require('jsonwebtoken');
const {
    User,
} = require('../connection');
const { createErrorResponse } = require('../helpers/responseweb');

module.exports = function (req, res, next) {
    let token = req.headers['x-access-token'] || req.headers['authorization'] || req.headers['Authorization']; // Express headers are auto converted to lowercase
    const isRefreshToken = req.headers['isrefreshtoken'];

    if (token === undefined) {
        return createErrorResponse(req, res, 'Unauthorized Access!', { 'message': 'authorize token missing' }, 401);
    }

    if (token.startsWith('Bearer')) {
        token = token.slice(7, token.length);
    }
    if (token) {
        jwt.verify(token, process.env.secret, async (err, decoded) => {
            if (err) {
                if (err.name == "TokenExpiredError") {
                    if (isRefreshToken) {
                        const payload = jwt.verify(token, process.env.secret, { ignoreExpiration: true }, async (err, decodede) => {
                            const userInfo = await User.findOne({
                                where: { tokenData: decodede.tokendata }
                            });
                            
                            if (userInfo) {
                                req.decoded = userInfo.dataValues;
                                next();
                            } else {
                                return createErrorResponse(req, res, 'Unauthorized Access!', { 'message': 'user not found' }, 401);
                            }
                        });

                    } else {
                        return createErrorResponse(req, res, 'Unauthorized Access!', { 'message': 'Unauthorized Access!', "isExpired": 1 }, 401);
                    }

                } else {

                    return createErrorResponse(req, res, 'Unauthorized Access!', { 'message': 'Unauthorized Access!' }, 401);
                }

            } else {
                const userInfo = await User.findOne({
                    where: { tokenData: decoded.tokendata }
                });
                if (userInfo === null) {
                    return createErrorResponse(req, res, 'Unauthorized Access!', { 'message': 'Unauthorized Access!' }, 401);
                } else {
                    
                    if (userInfo.dataValues.status == 1) {
                        return createErrorResponse(req, res, 'Your Account has been blocked', { 'account': 'Your Account has been blocked.' }, 422);
                    } else {
                        req.decoded = userInfo.dataValues;
                        next();
                    }
                }
            }
        });
    } else {
        return createErrorResponse(req, res, 'Unauthorized Access!', { 'message': 'authorize token missing' }, 401);
    }
};
