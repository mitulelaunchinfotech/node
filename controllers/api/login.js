const express = require('express');
const md5 = require('md5');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');

const {
    sequelize,
    User
} = require('../../connection')

const {
    createUserValidator,
    loginUservalidator
} = require('../../helpers/validators/createUser');


const {
    createSuccessResponse,
    createErrorResponse,
    DBConnection,
} = require("../../helpers/responseweb");

const userregister = (async (req, res) => {

    const validationResult = createUserValidator(req.body, req, res);
    if (validationResult.status) {
        return createErrorResponse(req, res, validationResult.message, validationResult.error, 422);
    }

    const {
        first_name,
        last_name,
        email,
        password
    } = req.body;

    // const doesExit = await User.findOne( {
    //     where:{
    //         email : email
    //     }
    // }).then((userData) => {
    //     return doesExit = userData;
    // });

    // if (doesExit != null) {
    //     return createErrorResponse(req, res, 'Email id already registered', { "email": "Email id already registered" }, 422);
    // }
    var checkUserEmail = await User.findOne({
        where: {
            email: email,
        }
    }).then((userData) => {
        return checkUserEmail = userData;
    });

    if (checkUserEmail != null) {
        return createErrorResponse(req, res, 'Email id already registered', {
            "email": "Email id already registered"
        }, 422);
    }

    try {

        let ja = await User.create({
            first_name: first_name,
            last_name: last_name,
            email: email,
            password: md5(password)
        });
        return createSuccessResponse(res, "User Register Successfully");
    } catch (err) {
        return createErrorResponse(req, res, "Internal Server error", 501);
    }
});


const userlogin = (async (req, res) => {
    
    const validationResult = loginUservalidator(req.body, req, res);
    if (validationResult.status) {
        return createErrorResponse(req, res, validationResult.message, validationResult.error, 422);
    }

    const {
        email,
        tokendata,
        password
    } = req.body;

    userInfo = await User.findOne({
        where: {
            email: email,
        }
    }).then((userData) => {
        userInfo = userData;
        return userData;
    });

    if (userInfo == null) {
        return createErrorResponse(req, res, 'Email not found', {
            "email": "Email not found"
        }, 422);
    } else {

        let tokendata = md5(Math.floor(Math.random() * 9000000000) + 1000000000) + md5(new Date(new Date().toUTCString()));
       
        let ja = await User.update({
            tokenData: tokendata
        }, {
            where: {
                id: userInfo.id
            }
        });

        let token = jwt.sign({
            tokendata
        }, process.env.secret, {
            expiresIn: '24h'
        });

        let userId = userInfo.dataValues.id;

        response = {
            user_id: userInfo.dataValues.id,
            first_name: userInfo.dataValues.first_name,
            last_name: userInfo.dataValues.last_name,
            email: userInfo.dataValues.email,
            token: token,
        }
        return createSuccessResponse(res, "Login Successfully", response);
    }
})

const logout = (async (req,res)=>{ 
    
    const userInfo = req.decoded;

    var uid = userInfo.id;

    let userupdatdata;
    userupdatdata = {
        tokenData: '',
        device_token:''
    }

    userupdatInfo = await User.update(userupdatdata, {
        where: {
            id: uid
        }
    });

    if(req.session){
        req.session.destroy();
    }
    return createSuccessResponse(res, "Logout Successfully", {});
})

module.exports = {
    userregister,
    userlogin,
    logout
}