const express = require("express");
const db = require('../../connection');
const { Sequelize  } = require('sequelize');
const bodyParser = require('body-parser');
const NodeTable = require('nodetable');
const md5 = require('md5');

const {
    sequelize,User
} = require('../../connection');

const {
     createSuccessResponse,
     createErrorResponse, 
     DBConnection,
 }
     = require("../../helpers/responseweb");


const index = (async (req,res)=> {  
    res.render('user/create',{
    });
});


const create = (async function (req, res) {
    const {
        first_name,
        last_name,
        email,
        password
    } = req.body;

    try {
        let ja = await User.create({
            first_name: first_name,
            last_name: last_name,
            email: email,
            password: md5(password)
        });
        return createSuccessResponse(res, "Insert Successfully");
    } catch (err) {
        return createErrorResponse(req,res, "Internal Server error", err,200);
    }
});

const view = (async (req,res)=> {
    res.render('user/view')    
});

const userlist = (async (req,res)=> {
    const db = DBConnection();
    const requestQuery = req.query;
    let columnsMap = [{
        db: "id",
        dt: 0
    },
    {
        db: "first_name" ,
        dt: 1
    },
    {
        db: "last_name",
        dt: 2
    },

    {
        db: "email",
        dt: 3
    },
    {
        db: "createdAt",
        dt: 4
    },
    ];

    const primaryKey = "id";
    const query = "SELECT * from users"
    const nodeTable = new NodeTable(requestQuery, db, query, primaryKey, columnsMap);  
    nodeTable.output((err, data) => {
       
        if(err)
        {
            console.log(err);
            return;
        }
        for(var i=data.data.length - 1;i>=0;i--)
        {   
            var action = "";
            action += "<button  class='btn btn-sm btn-primary cat-edit ml-2' data-id='" + data.data[i][0] + "' title='Click to Edit' data-bs-toggle='modal' data-bs-target='#kt_modal_new_target' ><i class='uil-edit'></i>Edit</button>";
            action += "<button class='btn btn-sm btn-danger cat-del ml-2' data-id='"+data.data[i][0]+"' title='Click to Delete'><i class='uil-trash-alt'></i>Delete</button>"; 
            data.data[i][4] = action;
        }
        
        res.send(data)
    })  
});

const userdelete = (async(req,res)=>{
    const {
        id,
    } = req.body;
    try {
        let ja = await User.destroy({
            where: {
                id: id
            }
        });
    }
        catch(err){
            return '0';
        }
});

const edituser = (async(req,res)=>{

    try{
        let user = await User.findOne({
            where:{
                id:req.query.id,
            }
        });
        res.send(user);
    }catch(err){
        return 0;
    }
})

const updateuser = (async(req,res)=>{
    const {
        user_id,
        first_name,
        last_name,
        email,
    } = req.body;

    try{
        let ja = await User.update({
            first_name:first_name,
            last_name:last_name,
            email:email
        },{
            where:{
                id:user_id
            }
        });
        return createSuccessResponse(res, "Update Successfully");
    }catch(err){
        return createErrorResponse(req,res, "Error");

    }
});

module.exports = {index,create,view,userlist,userdelete,edituser,updateuser}