const express = require('express');

const {
    sequelize,User
} = require('../../connection');



const adminLogin = (async (req,res)=> {
    res.render('login', {
        title: 'Login',
        login_nav: 'active',
        message: req.flash('loginMessage'),
        req
    });
});


const logout = (async (req,res)=>{
    req.logout();
    res.redirect('/');
})


const forgetpassword = (async (req,res)=>{
    
})
module.exports = { adminLogin,logout,forgetpassword }

