const express = require('express');
const passport = require('passport');
const { route } = require('express/lib/application');
const router  = express.Router();

const AdminLoginController = require('../controllers/admin/login');
const AdminUserController = require('../controllers/admin/user');


const verifyToken = require('../middlewares/index');


//API
const LoginControllerAPI = require('../controllers/api/login')

router.get('/',AdminLoginController.adminLogin)
router.post('/',passport.authenticate('admin-local',{
    successRedirect:  'index',
    failureRedirect: '/',
    failureFlash: true
   
}))

router.get('/logout',AdminLoginController.logout)
router.get('/forgetpassword',AdminLoginController.forgetpassword)


router.get('/index', AdminUserController.index) 
router.post('/create', AdminUserController.create)  
router.get('/view', AdminUserController.view) 
router.get('/user-list', AdminUserController.userlist);
router.post('/user-delete',AdminUserController.userdelete)
router.get('/user-edit',AdminUserController.edituser)
router.post('/update-user',AdminUserController.updateuser)



//API routes
router.post('/user-register',LoginControllerAPI.userregister);
router.post('/user-login',LoginControllerAPI.userlogin);
router.post('/logout',verifyToken,LoginControllerAPI.logout);




// router.prefix('/user', (route) => {
//     route.post('/add-category',  AdminUserController.category_add);
// })
    
module.exports = router;